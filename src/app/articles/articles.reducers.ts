import { createReducer, on } from '@ngrx/store';
import { ArticlesLoaded } from './articles.actions';

export const initialState = [];

const articlesReducer = createReducer(initialState,
  on(ArticlesLoaded, (state, { articles }) => articles )
);

export function reducer(state, action) {
  return articlesReducer(state, action);
}
