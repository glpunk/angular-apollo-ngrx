import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, catchError, exhaustMap } from 'rxjs/operators';
import { ArticlesService } from './articles.service';
import * as ArticlesActions from './articles.actions';

export interface ArticleResult {
  data: string;
  category: object;
  image: object;
}

@Injectable()
export class ArticlesEffects {

  loadArticles$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ArticlesActions.loadArticles),
      exhaustMap(() => this.articlesService.getAll()
        .pipe(
          map( articles => ArticlesActions.ArticlesLoaded({ articles }),
          catchError(() => of(ArticlesActions.ArticlesLoadError()))
        )
      )
    )
  ));

  constructor(
    private actions$: Actions,
    private articlesService: ArticlesService
  ) { }
}
