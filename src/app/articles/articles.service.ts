import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import ARTICLES_QUERY from '../apollo/queries/article/articles';
import { Subscription, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Article } from './../article/article.model';

@Injectable()
export class ArticlesService {
  constructor(private http: HttpClient, private apollo: Apollo) {}

  getAll(): Observable<Article[]> {
    return this.apollo
      .watchQuery<any>({
        query: ARTICLES_QUERY
      })
      .valueChanges.pipe(map(({data}) => data.articles));
  }
}
