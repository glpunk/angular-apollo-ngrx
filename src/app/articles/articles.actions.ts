import { createAction, props } from '@ngrx/store';
import { Article } from './../article/article.model';

export const loadArticles = createAction('[Articles Component] loadArticles');
export const ArticlesLoaded = createAction('[Articles Component] ArticlesLoaded',
props<{ articles: Article[] }>());
export const ArticlesLoadError = createAction('[Articles Component] ArticlesLoadError');
