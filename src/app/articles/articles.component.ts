import { Component, OnInit, OnDestroy } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { Observable } from 'rxjs';

import { Store } from '@ngrx/store';
import { loadArticles } from './articles.actions';
import { Article } from './../article/article.model';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit, OnDestroy {
  data: any = {};
  loading = true;
  errors: any;
  leftArticlesCount: any;
  leftArticles: any[];
  rightArticles: any[];

  articles$: Observable<Article[]> = this.store.select(state => state.articles);

  constructor(private apollo: Apollo, private store: Store<{ articles: Article[] }>) {}

  ngOnInit() {
    this.store.dispatch(loadArticles());

    this.articles$.subscribe(result => {
      this.data = result;
      this.leftArticlesCount = Math.ceil(this.data.length / 5);
      this.leftArticles = this.data.slice(0, this.leftArticlesCount);
      this.rightArticles = this.data.slice(
        this.leftArticlesCount,
        this.data.length
      );
    });
  }

  ngOnDestroy() {}
}
