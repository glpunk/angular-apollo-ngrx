export interface Article {
  id: number;
  title: string;
  category: object;
  image: object;
}